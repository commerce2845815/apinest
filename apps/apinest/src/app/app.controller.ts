import { Controller, Get, Req,Logger } from '@nestjs/common';

import { AppService } from './app.service';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  getData(@Req() req:Request) {
    console.log(`request is${JSON.stringify(req.method)}`,AppController.name)
    Logger.log('request is',req)
    return this.appService.getData();
  }
}
