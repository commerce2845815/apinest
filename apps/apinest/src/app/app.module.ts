import { Module ,Logger} from '@nestjs/common';

import { AppController } from './app.controller';
import { AppService } from './app.service';
import {  MiddlewareConsumer } from '@nestjs/common';
import { MorganMiddleware } from '@nest-middlewares/morgan';

@Module({
  imports: [],
  controllers: [AppController],
  providers: [AppService,Logger],
})
export class AppModule {
   configure(consumer: MiddlewareConsumer) {
    // IMPORTANT Call Middleware.configure BEFORE using it for routes
    
      MorganMiddleware.configure({
  format: 'combined',
  stream: {
    write: (message) => {
      // Assuming `logger` is an instance of your custom logger
      Logger.verbose(message);
    },
  },
}as any);
    consumer.apply(MorganMiddleware).forRoutes('');
  }
}
