import { createLogger, format, transports } from "winston";

// custom log display format
const customFormat = format.printf(({timestamp, level, stack, message}) => {
    return `${timestamp} - [${level.toUpperCase().padEnd(7)}] - ${stack || message}`
})

const options = {
    file: {
        filename: 'error.log',
        level: 'error'
    },
    console: {
        level: 'silly',
         colorize: true,
    }
}

// for development environment
const devLogger = {
    format: format.combine(
        format.timestamp({ format: 'DD-MM-YYYY, HH:mm:ss' }),
        format.json(), 
        format.printf(({ timestamp, level, message, context, trace }) => {
        return `${timestamp} [${context}] ${level}: ${message}${trace ? `\n${trace}` : ''}`;
      }),
         format.colorize({ all: true }),
        
    ),
    transports: [new transports.Console(options.console),new transports.File(options.file)]
}

// for production environment
const prodLogger = {
    format: format.combine(
       
        format.timestamp(),
        format.errors({stack: true}),
        format.json(),
         format.colorize({ all: true }),
    ),
    transports: [
        new transports.File(options.file),
        new transports.File({
            filename: 'combine.log',
            level: 'info'
        })
    ]
}

// export log instance based on the current environment
const instanceLogger = (process.env.NODE_ENV === 'production') ? prodLogger : devLogger

export const instance = createLogger(instanceLogger)
