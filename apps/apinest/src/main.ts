/**
 * This is not a production server yet!
 * This is only a minimal backend to get started.
 */

import { Logger } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';

import { AppModule } from './app/app.module';
// imports for Winston logger
import { WinstonModule,WinstonLogger } from 'nest-winston';
import { instance } from './winston.logger';
import morgan from 'morgan';

async function bootstrap() {
  const app = await NestFactory.create(AppModule, {
    logger: WinstonModule.createLogger({
      instance: instance,
    }),
    });
  const globalPrefix = 'api';
  app.setGlobalPrefix(globalPrefix);
       
  app.use(morgan('combined'));

  const port = process.env.PORT || 3000;
  await app.listen(port);
  Logger.log(
    `🚀 Application is running on: http://localhost:${port}/${globalPrefix}`,'main'

  );
}

bootstrap();
