import { Module, MiddlewareConsumer, Logger } from '@nestjs/common';
import { MorganMiddleware } from '@nest-middlewares/morgan';

@Module({
  // Your module metadata here
})
export class MyModule {
  configure(consumer: MiddlewareConsumer) {
    // IMPORTANT Call Middleware.configure BEFORE using it for routes
    
      MorganMiddleware.configure({
  format: 'combined',
  stream: {
    write: (message) => {
      // Assuming `logger` is an instance of your custom logger
      Logger.verbose(message);
    },
  },
}as any);
    consumer.apply(MorganMiddleware).forRoutes(/* your routes */);
  }
}